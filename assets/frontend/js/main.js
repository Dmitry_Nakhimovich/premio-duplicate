$(document).ready(function () {

    $('form .dropdown-menu a').click(function () {
        var btn = $(this).parents('.dropdown').find('.dropdown-btn');
        var field = $(btn).data('field');
        $(btn).html($(this).html() + '<span class="caret"></span>');
        $(field).val($(this).html());
        $('.form-dropdown').removeClass('open');
        $('.form-dropdown').find('.dropdown-backdrop').remove();
        return false;
    });

    $("input.inputDay").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus',
        min: 0,
        max: 31,
        step: 1,
        boostat: 2,
        maxboostedstep: 5
    });
    $("input.inputTime1").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus',
        min: 0,
        max: 23,
        step: 1,
        boostat: 2,
        maxboostedstep: 5
    });
    $("input.inputTime2").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus',
        min: 00,
        max: 59,
        step: 01,
        boostat: 5,
        maxboostedstep: 15
    });
    $("input.inputPeople").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'glyphicon glyphicon-plus',
        verticaldownclass: 'glyphicon glyphicon-minus',
        min: 1,
        max: 200,
        step: 1,
        boostat: 2,
        maxboostedstep: 4
    });

    $('.search-link').magnificPopup({
        items: {
            src:
                '<div id="popup-search" class="search-popup">' +
                    '<form method="GET" action="search">' +
                        '<div class="title">Поиск по сайту</div>' +
                        '<div class="input-group">' +
                            '<input type="text" name="search" class="form-control" placeholder="Что будем искать?" required>' +
                            '<span class="input-group-btn">' +
                                '<button class="btn btn-default" type="submit">НАЙТИ</button>' +
                            '</span>' +
                        '</div>' +
                    '</form>' +
                '</div>',
            type: 'inline'
        },
        preloader: false,
        closeBtnInside: true,
        fixedContentPos: true,
        mainClass: 'mfp-fade',
        removalDelay: 300
    });

    jQuery(function($){
      $("#inputTel").mask("?+7 (999) 999-9999");
    });

    if ($('body').hasClass('events_detail')) {
        $('.image-popup').magnificPopup({
            type: 'image',
            disableOn: function() {
                if( $(window).width() < 768 ) {
                    return false;
                }
                return true;
            },
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });
    }

    if ($('body').hasClass('monet_index')) {
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeBtnInside: true,
            fixedContentPos: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,

            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });
        $("input[name='inputDay']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 0,
            max: 31,
            step: 1,
            boostat: 2,
            maxboostedstep: 5
        });
        $("input[name='inputTime1']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 0,
            max: 23,
            step: 1,
            boostat: 2,
            maxboostedstep: 5
        });
        $("input[name='inputTime2']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 00,
            max: 59,
            step: 01,
            boostat: 5,
            maxboostedstep: 15
        });
        $("input[name='inputPeople']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 1,
            max: 200,
            step: 1,
            boostat: 2,
            maxboostedstep: 4
        });
    }

    if ($('body').hasClass('premio_index')) {
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeBtnInside: true,
            fixedContentPos: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,

            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });
    }

    if ($('body').hasClass('terrace_index')) {
        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeBtnInside: true,
            fixedContentPos: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,

            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });
        $("input[name='inputDay']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 0,
            max: 31,
            step: 1,
            boostat: 2,
            maxboostedstep: 5
        });
        $("input[name='inputTime1']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 0,
            max: 23,
            step: 1,
            boostat: 2,
            maxboostedstep: 5
        });
        $("input[name='inputTime2']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 00,
            max: 59,
            step: 01,
            boostat: 5,
            maxboostedstep: 15
        });
        $("input[name='inputPeople']").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'glyphicon glyphicon-plus',
            verticaldownclass: 'glyphicon glyphicon-minus',
            min: 1,
            max: 200,
            step: 1,
            boostat: 2,
            maxboostedstep: 4
        });
    }

    if ($('body').hasClass('gallery_detail')) {
        var initPhotoSwipeFromDOM = function(gallerySelector) {

            // parse slide data (url, title, size ...) from DOM elements
            // (children of gallerySelector)
            var parseThumbnailElements = function(el) {
                var thumbElements = el.childNodes,
                    numNodes = thumbElements.length,
                    items = [],
                    figureEl,
                    linkEl,
                    size,
                    item;

                for(var i = 0; i < numNodes; i++) {

                    figureEl = thumbElements[i]; // <figure> element

                    // include only element nodes
                    if(figureEl.nodeType !== 1) {
                        continue;
                    }

                    linkEl = figureEl.children[0]; // <a> element

                    size = linkEl.getAttribute('data-size').split('x');

                    // create slide object
                    item = {
                        src: linkEl.getAttribute('href'),
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };



                    if(figureEl.children.length > 1) {
                        // <figcaption> content
                        item.title = figureEl.children[1].innerHTML;
                    }

                    if(linkEl.children.length > 0) {
                        // <img> thumbnail element, retrieving thumbnail url
                        item.msrc = linkEl.children[0].getAttribute('src');
                    }

                    item.el = figureEl; // save link to element for getThumbBoundsFn
                    items.push(item);
                }

                return items;
            };

            // find nearest parent element
            var closest = function closest(el, fn) {
                return el && ( fn(el) ? el : closest(el.parentNode, fn) );
            };

            // triggers when user clicks on thumbnail
            var onThumbnailsClick = function(e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;

                var eTarget = e.target || e.srcElement;

                // find root element of slide
                var clickedListItem = closest(eTarget, function(el) {
                    return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                });

                if(!clickedListItem) {
                    return;
                }

                // find index of clicked item by looping through all child nodes
                // alternatively, you may define index via data- attribute
                var clickedGallery = clickedListItem.parentNode,
                    childNodes = clickedListItem.parentNode.childNodes,
                    numChildNodes = childNodes.length,
                    nodeIndex = 0,
                    index;

                for (var i = 0; i < numChildNodes; i++) {
                    if(childNodes[i].nodeType !== 1) {
                        continue;
                    }

                    if(childNodes[i] === clickedListItem) {
                        index = nodeIndex;
                        break;
                    }
                    nodeIndex++;
                }



                if(index >= 0) {
                    // open PhotoSwipe if valid index found
                    openPhotoSwipe( index, clickedGallery );
                }
                return false;
            };

            // parse picture index and gallery index from URL (#&pid=1&gid=2)
            var photoswipeParseHash = function() {
                var hash = window.location.hash.substring(1),
                    params = {};

                if(hash.length < 5) {
                    return params;
                }

                var vars = hash.split('&');
                for (var i = 0; i < vars.length; i++) {
                    if(!vars[i]) {
                        continue;
                    }
                    var pair = vars[i].split('=');
                    if(pair.length < 2) {
                        continue;
                    }
                    params[pair[0]] = pair[1];
                }

                if(params.gid) {
                    params.gid = parseInt(params.gid, 10);
                }

                return params;
            };

            var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                var pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options,
                    items;

                items = parseThumbnailElements(galleryElement);

                // define options (if needed)
                options = {

                    // define gallery index (for URL)
                    galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                    getThumbBoundsFn: function(index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect();

                        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                    },

                    shareEl: false,
                    bgOpacity: 0.8

                };

                // PhotoSwipe opened from URL
                if(fromURL) {
                    if(options.galleryPIDs) {
                        // parse real index when custom PIDs are used
                        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                        for(var j = 0; j < items.length; j++) {
                            if(items[j].pid == index) {
                                options.index = j;
                                break;
                            }
                        }
                    } else {
                        // in URL indexes start from 1
                        options.index = parseInt(index, 10) - 1;
                    }
                } else {
                    options.index = parseInt(index, 10);
                }

                // exit if index not found
                if( isNaN(options.index) ) {
                    return;
                }

                if(disableAnimation) {
                    options.showAnimationDuration = 0;
                }

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            };

            // loop through all gallery elements and bind events
            var galleryElements = document.querySelectorAll( gallerySelector );

            for(var i = 0, l = galleryElements.length; i < l; i++) {
                galleryElements[i].setAttribute('data-pswp-uid', i+1);
                galleryElements[i].onclick = onThumbnailsClick;
            }

            // Parse URL and open gallery if it contains #&pid=3&gid=1
            var hashData = photoswipeParseHash();
            if(hashData.pid && hashData.gid) {
                openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
            }
        };
        // execute above function
        initPhotoSwipeFromDOM('.gallery-wrap');
    }

    if ($('body').hasClass('menu')) {
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            navClass: ['owl-btn owl-carousel-left','owl-btn owl-carousel-right'],
            navText: ['<div class="nav-arrow nav-left"></div>', '<div class="nav-arrow nav-right"></div>'],
            dots: true,
            items: 1
        });

        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeBtnInside: true,
            fixedContentPos: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,

            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });


        $('.image-popup').magnificPopup({
            type: 'image',
            disableOn: function() {
                if( $(window).width() < 768 ) {
                    return false;
                }
                return true;
            },
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });

    }

    if ($('body').hasClass('monet_rent')) {
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            navClass: ['owl-btn owl-carousel-left','owl-btn owl-carousel-right'],
            navText: ['<div class="nav-arrow nav-left"></div>', '<div class="nav-arrow nav-right"></div>'],
            dots: true,
            items: 1
        });

        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeBtnInside: true,
            fixedContentPos: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,

            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });

        $('.image-popup').magnificPopup({
            type: 'image',
            disableOn: function() {
                if( $(window).width() < 768 ) {
                    return false;
                }
                return true;
            },
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });
    }

    if ($('body').hasClass('premio_rent')) {
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            navClass: ['owl-btn owl-carousel-left','owl-btn owl-carousel-right'],
            navText: ['<div class="nav-arrow nav-left"></div>', '<div class="nav-arrow nav-right"></div>'],
            dots: true,
            items: 1
        });

        $('.popup-with-form').magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#name',
            closeBtnInside: true,
            fixedContentPos: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,

            callbacks: {
                beforeOpen: function() {
                    if($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#name';
                    }
                }
            }
        });

        $('.image-popup').magnificPopup({
            type: 'image',
            disableOn: function() {
                if( $(window).width() < 768 ) {
                    return false;
                }
                return true;
            },
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });
    }

    //Удаление выбора зала в террасе  добавление скрытого поля ,откуда пришёл заказ
    // if(window.location.pathname == "/terrace"){
    //     $('#place').addClass('hidden');
    //     $('#from').val('терраса');
    // }else {
    //     $('#place').removeClass('hidden');
    //     $('#from').val('ресторан монет');
    // };


});
